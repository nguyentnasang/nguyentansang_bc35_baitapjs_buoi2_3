// bài 1: Tính tiền lương nhân viên
function salary() {
    var input__salary = document.getElementById('input__salary').value;
    var day__salary = 100000;
    var total__salary = input__salary * day__salary;
    var result__lesson1 = document.getElementById('result__lesson1').innerHTML = `Số tiền lương trong ${input__salary} ngày là: ${total__salary}`;
    result__lesson1 = document.getElementById('result__lesson1').style.padding = '10px';
}
// bài 2: tính giá trị trung bình \
function average_value() {
    var number1__lesson2 = document.getElementById('number1__lesson2 ').value;
    var number2__lesson2 = document.getElementById('number2__lesson2 ').value;
    var number3__lesson2 = document.getElementById('number3__lesson2 ').value;
    var number4__lesson2 = document.getElementById('number4__lesson2 ').value;
    var number5__lesson2 = document.getElementById('number5__lesson2 ').value;
    var total__lesson2 = (parseInt(number1__lesson2) + parseInt(number2__lesson2) + parseInt(number3__lesson2) + parseInt(number4__lesson2) + parseInt(number5__lesson2)) / 5;

    var result__lesson2 = document.getElementById('result__lesson2 ').innerHTML = `Giá trị trung bình là: ${total__lesson2}`
    var result__lesson2 = document.getElementById('result__lesson2 ').style.padding = '10px';
}
// bài 3: quy đỏi tiền
function price() {
    var price__vnd = 23500;
    var price__usd = document.getElementById('price__usd ').value;
    var total__lesson3 = price__vnd * price__usd;
    var result__lesson3 = document.getElementById('result__lesson3 ').innerHTML = total__lesson3 + 'VNĐ'
    var result__lesson3 = document.getElementById('result__lesson3 ').style.padding = '10px';
}
// bài 4: Tính diện tích chu vi hình chữ nhật
function geometry() {
    var long = document.getElementById('long ').value;
    var wide = document.getElementById('wide ').value;
    var result__area__lesson4 = document.getElementById('result__area__lesson4 ').innerHTML = `Diện tích: ${long * wide}`;
    var result__perimeter__lesson4 = document.getElementById('result__perimeter__lesson4 ').innerHTML = `Chu vi: ${(parseInt(long) + parseInt(wide)) * 2}`
    var result__area__lesson4 = document.getElementById('result__area__lesson4 ').style.padding = "10px";
    var result__perimeter__lesson4 = document.getElementById('result__perimeter__lesson4 ').style.padding = "10px";

}
// bài 5: tính tổng 2 ký số
function numbers() {
    var numbers = document.getElementById("numbers ").value;
    var numbers__1 = Math.floor(numbers / 10);
    var numbers__2 = Math.floor(numbers % 10);
    console.log(numbers__1, numbers__2);
    var result__lesson5 = document.getElementById('result__lesson5 ').innerHTML = `Tổng 2 ký số là: ${numbers__1 + numbers__2}`;
    var result__lesson5 = document.getElementById('result__lesson5 ').style.padding = '10px';
}